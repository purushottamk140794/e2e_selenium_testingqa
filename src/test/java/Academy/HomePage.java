package Academy;

import PageObject.ForgotPassPage;
import PageObject.LandingPage;
import PageObject.LoginPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import resources.BaseClass;

import java.io.IOException;

public class HomePage extends BaseClass {
    private static Logger log = LogManager.getLogger(HomePage.class.getName());
    WebDriver driver;

    @BeforeTest
    public void initialize() throws IOException {
        driver = initializeDriver();
        log.info("Driver is Initialize");
    }

    @Test(dataProvider = "getData")
    public void navigateToHomePage(String userName, String pass) throws IOException {
        log.info("******************Inside login page have two different set of credentials**************");
        System.out.println("******************Inside login page have two different set of credentials**************");
        driver.get(properties.getProperty("url"));
        LandingPage lp = new LandingPage(driver);
        LoginPage loginPage = lp.getLogin();
        loginPage.getUser_email().sendKeys(userName);
        loginPage.getUser_pass().sendKeys(pass);
        loginPage.getsubmit().click();
        ForgotPassPage fp = loginPage.getForgotPass();
        fp.getuser_email().sendKeys("xxx@gmail.com");
        fp.getSend_Me_Instruction().click();

        log.info("TestCase successfully executed");
    }

    @DataProvider
    public Object[][] getData() {

        Object[][] data = new Object[2][2];
        data[0][0] = "nonrestricteduser@gmail.com";
        data[0][1] = "123456";
        //data [0][2]="non restricted user";

        data[1][0] = "restricteduser@gmail.com";
        data[1][1] = "1223456";
        // data [1][2]="restricted user";

        return data;
    }

    @AfterTest
    public void tearDown() {
        driver.close();
        log.info("closing the browser");
    }

}
