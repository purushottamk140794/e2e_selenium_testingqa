package Academy;

import PageObject.LandingPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import resources.BaseClass;

import java.io.IOException;

public class ValidateNavigationBar extends BaseClass {
    private static Logger log = LogManager.getLogger(ValidateNavigationBar.class.getName());
    WebDriver driver;
    @BeforeTest
    public void initialize() throws IOException {
        driver=initializeDriver();
        driver.get(properties.getProperty("url"));
        log.info("Driver is Initialize");
    }

    @Test
    public void navBarHomePage() throws IOException {

        LandingPage lp = new LandingPage(driver);
        System.out.println("***********Assertion of Navigation Bar*************");
        Assert.assertTrue(lp.getNavBar().isDisplayed());
        log.info("Navigation Bar is Displayed");
    }

    @AfterTest
    public void tearDown()
    {
        driver.close();
        log.info("closing the browser");
    }
}
