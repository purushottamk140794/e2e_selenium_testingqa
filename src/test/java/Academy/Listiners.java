package Academy;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import resources.BaseClass;
import resources.ExtendReportNG;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Listiners extends BaseClass implements ITestListener {
    ExtentReports extentReports =ExtendReportNG.config();
    ExtentTest extentTest;
    ThreadLocal<ExtentTest> testThreadLocal=new ThreadLocal<ExtentTest>();
     public void onTestStart(ITestResult result) {

         extentTest=extentReports.createTest(result.getMethod().getMethodName());
         testThreadLocal.set(extentTest);
    }

    public void onTestSuccess(ITestResult result) {

         testThreadLocal.get().log(Status.PASS,"Test Passed");
    }

    public void onTestFailure(ITestResult result)
    {
        WebDriver driver=null;
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        testThreadLocal.get().fail(result.getThrowable());
        String testMethodName=result.getMethod().getMethodName();
        try {
           driver= (WebDriver)result.getTestClass().getRealClass().getDeclaredField("driver").get(result.getInstance());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        try {
            System.out.println("Destination is "+getScreenshotPath(testMethodName,driver));
            testThreadLocal.get().addScreenCaptureFromPath(getScreenshotPath(testMethodName,driver));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public  void onTestSkipped(ITestResult result) {
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
    }

    public void onTestFailedWithTimeout(ITestResult result) {
        this.onTestFailure(result);
    }

    public void onStart(ITestContext context) {
    }

    public void onFinish(ITestContext context) {
        extentReports.flush();
    }
}
