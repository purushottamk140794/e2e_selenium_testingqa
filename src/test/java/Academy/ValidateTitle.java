package Academy;

import PageObject.LandingPage;
import PageObject.LoginPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import resources.BaseClass;

import java.io.IOException;

public class ValidateTitle extends BaseClass {
    private static Logger log = LogManager.getLogger(ValidateTitle.class.getName());
    WebDriver driver;
    @BeforeTest
    public void initialize() throws IOException {
        driver=initializeDriver();
        driver.get(properties.getProperty("url"));
        log.info("Driver is Initialize");
    }
   @Test()
    public void validateTitle() throws IOException {
       LandingPage lp=new LandingPage(driver);
       log.info("***********Assertion of Center Text*************");
       System.out.println("***********Assertion of Center Text*************");
       Assert.assertEquals(lp.getTextCenter().getText(),"feature courses");
       log.info("Title is validated");
    }
    @AfterTest
    public void tearDown()
    {
        driver.close();
        log.info("closing the browser");
    }
}
