package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import resources.BaseClass;

public class LoginPage extends BaseClass {

    public WebDriver driver;
    By user_email = By.cssSelector("#user_email");
    By user_pass = By.cssSelector("#user_password");
    By submit = By.xpath("//div[@class='form-group text-center']//input");
    By forgotPass = By.cssSelector(".link-below-button");

    public LoginPage(WebDriver driver) {

        this.driver = driver;
    }

    public WebElement getUser_email() {
        return driver.findElement(user_email);
    }

    public WebElement getUser_pass() {
        return driver.findElement(user_pass);
    }

    public WebElement getsubmit() {
        return driver.findElement(submit);
    }

    public ForgotPassPage getForgotPass() {
        driver.findElement(forgotPass).click();
        return new ForgotPassPage(driver);
    }

}
