package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import resources.BaseClass;

public class LandingPage extends BaseClass {

    public WebDriver driver;
    By sign_in=By.cssSelector("a[href*='sign_in']");
    By textcenter=By.xpath("//h2[text()='Featured Courses']");
    By navbar= By.cssSelector(".nav.navbar-nav.navbar-right li a");
    public LandingPage(WebDriver driver) {
      this.driver=driver;
    }

    public LoginPage getLogin()
    {
        driver.findElement(sign_in).click();
        return new LoginPage(driver);
    }
    public WebElement getTextCenter()
    {
        return driver.findElement(textcenter);
    }

    public WebElement getNavBar()
    {
        return driver.findElement(navbar);
    }
}
