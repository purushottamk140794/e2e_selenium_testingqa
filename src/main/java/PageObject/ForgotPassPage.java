package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ForgotPassPage {
    WebDriver driver;
    By user_email = By.cssSelector("#user_email");
    By Send_Me_Instruction = By.xpath("//input[@type='submit']");

    ForgotPassPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getuser_email() {
        return driver.findElement(user_email);
    }

    public WebElement getSend_Me_Instruction() {
        return driver.findElement(Send_Me_Instruction);
    }

}
