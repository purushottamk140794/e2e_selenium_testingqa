package resources;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class BaseClass {
    public WebDriver driver;
    public Properties properties = new Properties();

    public WebDriver initializeDriver() throws IOException {
        properties = new Properties();
        FileInputStream fi = new FileInputStream(System.getProperty("user.dir") + "//src//main//java//resources//data.properties");
        properties.load(fi);
        String browserName = System.getProperty("Browser");
        //String browserName = properties.getProperty("Browser");
        if (browserName.equalsIgnoreCase("Chrome")) {
            System.setProperty("webdriver.crome.driver", System.getProperty("user.dir") + "//src//main//java//resources//chromedriver.exe");
            driver = new ChromeDriver();
        } else if (browserName.contains("Firefoxheadless")) {
            System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "//src//main//java//resources//geckodriver.exe");
            FirefoxOptions options = new FirefoxOptions();
            if (browserName.contains("headless")) {
                options.addArguments("--headless");
            }
            driver = new FirefoxDriver(options);
        } else if (browserName.equalsIgnoreCase("IE")) {
            System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "//src//main//java//resources//IEdriver.exe");
            driver = new InternetExplorerDriver();
        }
        return driver;
    }

    public String getScreenshotPath(String testMethodName, WebDriver driver) throws IOException {
        TakesScreenshot ts = (TakesScreenshot) driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        String destination = System.getProperty("user.dir") + "\\reports\\" + testMethodName + ".png";
        FileUtils.copyFile(source, new File(destination));
        return destination;
    }

}